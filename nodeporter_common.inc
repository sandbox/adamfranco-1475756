<?php
// $Id$

/**
 * @file
 * Common functions used by the nodeporter modules.
 */

/**
 * A cache for mapping information.
 * 
 * @copyright Copyright &copy; 2010, Middlebury College
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License (GPL)
 */
class NodeporterUserMap {
    
  private static $uidToMail = array();
  private static $uidsMissingMail = array();
  private static $unknownMail = array();
  private static $uidToName = array();
  private static $uidsMissingName = array();
  private static $unknownName = array();
  private static $unknownUids = array();
  
  /**
   * Answer the email address for a UID.
   *
   * Throws an Exception if an address is not found.
   * 
   * @param int $uid
   *    The UID
   * @return string
   *    The email address.
   */
  public static function mailFromUid ($uid) {
    if (!isset(self::$uidToMail[$uid])) {
      if (in_array($uid, self::$uidsMissingMail))
        throw new Exception(t('No email address found for user @uid.', array('@uid' => $uid)), 500);
      if (in_array($uid, self::$unknownUids))
        throw new Exception(t('Unknown user @uid.', array('@uid' => $uid)), 404);
      
      $row = db_fetch_array(db_query("SELECT mail, init FROM {users} WHERE uid = %d", $uid));
      if (!$row) {
        self::$unknownUids[] = $uid;
        throw new Exception(t('Unknown user @uid.', array('@uid' => $uid)), 404);
      }

      if ($row['mail'])
        self::$uidToMail[$uid] = $row['mail'];
      else if ($row['init'])
        self::$uidToMail[$uid] = $row['init'];
      else {
        self::$uidsMissingMail[] = $uid;  // Cache the fact that the mail is missing.
        throw new Exception(t('No email address found for user @uid.', array('@uid' => $uid)), 500);
      }
    }
    
    return self::$uidToMail[$uid];
  }
  
  /**
   * Answer the uid matching an email address.
   *
   * Throws an Exception if no uid matching the address is found.
   * 
   * @param string $email
   *    The email to search for
   * @return int
   *    The UID found.
   */
  public static function uidFromMail ($email) {
    if (!in_array($email, self::$uidToMail)) {
      if (in_array($email, self::$unknownMail))
        throw new Exception(t('No uid found for email @email.', array('@email' => $email)), 500);
      
      $uid = db_result(db_query("SELECT uid FROM {users} WHERE mail = '%s'", $email));
      if (!$uid)
        $uid = db_result(db_query("SELECT uid FROM {users} WHERE init = '%s'", $email));

      if ($uid)
        self::$uidToMail[$uid] = $email;
      else {
        self::$unknownMail[] = $email;  // Cache the fact that the mail is missing.
        throw new Exception(t('No uid found for email @email.', array('@email' => $email)), 500);
      }
    }
    
    return array_search($email, self::$uidToMail);
  }
  
  /**
   * Answer the name for a UID.
   *
   * Throws an Exception if a name is not found.
   * 
   * @param int $uid
   *    The UID
   * @return string
   *    The user name.
   */
  public static function nameFromUid ($uid) {
    if (!isset(self::$uidToName[$uid])) {
      if (in_array($uid, self::$uidsMissingName))
        throw new Exception(t('No name found for user @uid.', array('@uid' => $uid)), 500);
      if (in_array($uid, self::$unknownUids))
        throw new Exception(t('Unknown user @uid.', array('@uid' => $uid)), 404);
      
      $row = db_fetch_array(db_query("SELECT name FROM {users} WHERE uid = %d", $uid));
      if (!$row) {
        self::$unknownUids[] = $uid;
        throw new Exception(t('Unknown user @uid.', array('@uid' => $uid)), 404);
      }

      if ($row['name'])
        self::$uidToName[$uid] = $row['name'];
      else if ($row['init'])
        self::$uidToName[$uid] = $row['init'];
      else {
        self::$uidsMissingName[] = $uid;  // Cache the fact that the name is missing.
        throw new Exception(t('No name found for user @uid.', array('@uid' => $uid)), 500);
      }
    }
    
    return self::$uidToName[$uid];
  }
  
  /**
   * Answer the uid matching a name.
   *
   * Throws an Exception if no uid matching the name is found.
   * 
   * @param string $name
   *    The user-name to search for
   * @return int
   *    The UID found.
   */
  public static function uidFromName ($name) {
    if (!in_array($name, self::$uidToName)) {
      if (in_array($name, self::$unknownName))
        throw new Exception(t('No uid found for name @name.', array('@name' => $name)), 500);
      
      $uid = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $name));

      if ($uid)
        self::$uidToName[$uid] = $name;
      else {
        self::$unknownName[] = $name;  // Cache the fact that the name is missing.
        throw new Exception(t('No uid found for name @name.', array('@name' => $name)), 500);
      }
    }
    
    return array_search($name, self::$uidToName);
  }
  
  /**
   * Answer true if the uid passed exists, false otherwise.
   * 
   * @param int $uid
   *    The UID
   * @return boolean
   */
  public static function uidExists ($uid) {
    try {
      self::mailFromUid($uid);
      return true;
    } catch (Exception $e) {
      // UID not found
      if ($e->getCode() == 404)
        return false;
      // UID found, but no email
      else if ($e->getCode() == 500)
        return true;
      // Other error
      else
        throw new Exception(t('Could not determine if user @uid exists. Code @code: @error', array('@uid' => $uid, '@code' => $e->getCode(), '@error' => $e->getMessage())));
    }
  }
  
  /**
   * Answer a user object with uid and name based on one of three inputs.
   *
   * The priority of inputs is as follows:
   *    1. mail - Since mail will likely be the same between systems, this is used first.
   *              Will also match init if the mail isn't matched.
   *    2. name - Username will be matched second as this may be shared between systems.
   *    3. uid  - Since uid is interall to a Drupal installation it has least priority.
   *              To import only based on UID, eliminate mail and name attributes and only include the uid.
   *
   * Throws an exception if no user can be matched.
   * 
   * @param string $mail  An email address, init value, or null to be ignored.
   * @param string $name  A username or null to be ignored.
   * @param int $uid      An interal user id for this Drupal instance or null to be ignored.
   * @return array A user array with uid, name, and mail elements.
   */
  public static function getUserByMatch ($mail, $name, $uid) {
    $user = array();
    if (!is_null($mail)) {
      try {
        $user['uid'] = self::uidFromMail($mail);
        $user['name'] = self::nameFromUid($user['uid']);
        $user['mail'] = self::mailFromUid($user['uid']);
        return $user;
      } catch (Exception $e) {
      }
    }
    if (!is_null($name)) {
      try {
        $user['uid'] = self::uidFromName($name);
        $user['name'] = self::nameFromUid($user['uid']);
        $user['mail'] = self::mailFromUid($user['uid']);
        return $user;
      } catch (Exception $e) {
      }
    }
    if (!is_null($uid) && self::uidExists($uid)) {
      try {
        $user['uid'] = $uid;
        $user['name'] = self::nameFromUid($user['uid']);
        $user['mail'] = self::mailFromUid($user['uid']);
        return $user;
      } catch (Exception $e) {
      }
    }
    throw new Exception(t("No user found for mail '@mail', name '@name', or uid '@uid'.", array('@mail' => $mail, '@name' => $name, '@uid' => $uid)), 500);
  }
}

